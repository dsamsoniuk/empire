var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}


Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addStyleEntry('baseCss',[
        './assets/css/app.scss'
    ])
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .addEntry('baseJs',[
        './assets/js/app.js'
        ])
    .enableSassLoader()
    .enableTypeScriptLoader()
    .disableSingleRuntimeChunk()
;

module.exports = Encore.getWebpackConfig();
