<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Controller dla mapy wszystkich zamkow
 */
class WorldController extends AbstractController
{
    /**
     * @Route("/world", name="world")
     */
    public function index()
    {

        

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/WorldController.php',
        ]);
    }
}
