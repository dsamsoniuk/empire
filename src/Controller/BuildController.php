<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Lista budynkow do stworzenia/upgrade/usuwanie
 */
class BuildController extends AbstractController
{
    /**
     * @Route("/build", name="build")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/BuildController.php',
        ]);
    }
}
