<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * Lista surowcow do kupienia
 */
class TradeController extends AbstractController
{
    /**
     * @Route("/trade", name="trade")
     */
    public function index()
    {
        // return ['number' => 2323];
        return $this->render('trade/index.html.twig', [
            'number' => 23232,
        ]);
        // return new Response(
        //     '<html><body>Lucky number: 121</body></html>'
        // );
        // return $this->json([
        //     'message' => 'Welcome to your new controller!',
        //     'path' => 'src/Controller/TradeController.php',
        // ]);
    }
}
