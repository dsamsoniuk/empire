<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Lista mojich potyczek wojennych
 */
class BattleController extends AbstractController
{
    /**
     * @Route("/battle", name="battle")
     * @IsGranted({"ROLE_USER"})
     */
    public function index()
    {
        return $this->render('battle/index.html.twig', [
            'number' => 23232,
        ]);
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/BattleController.php',
        ]);
    }
}
