<?php

namespace App\Service;

// use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserMenu 
{
    // public function __construct(\Twig_Environment $templating)
    // {
    //     $this->templating = $templating;
    // }
    public function getTopBar()
    {
        $messages = [
            'You did it! You updated the system! Amazing!',
            'That was one of the coolest updates I\'ve seen all day!',
            'Great work! Keep going!',
        ];
        // $this->templating->render('base.html.twig');
        $index = array_rand($messages);
        // $this->container->get('templating')->render($view, $parameters);
        return [
            'message' => $messages[$index]
        ];
    }
}